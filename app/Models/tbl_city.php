<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_city extends Model
{
    protected $fillable = [
        'city_name',
    ];

    protected $primarykey = 'city_id';
}
