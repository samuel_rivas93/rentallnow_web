<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tbl_collection_img_tecnologias extends Model
{
    protected $fillable = [
        'collection_img_tecnologias_url',
        'tecnologias_id'
    ];

    protected $primarykey = 'collection_img_tecnologias_id';
}
