<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\tbl_collection_img_inmuebles;

class tbl_inmuebles extends Model
{
    protected $fillable = [
        'inmuebles_name',
        'inmuebles_lat',
        'inmuebles_lng',
        'inmuebles_date',
        'inmuebles_huespedes',
        'inmuebles_cama',
        'inmuebles_habitaciones',
        'inmuebles_baños',
        'inmuebles_privado',
        'inmuebles_modcons',
        'inmuebles_description',
        'inmuebles_price',
        'subcategory_id',
        'user_id'
    ];

    protected $primarykey = 'inmuebles_id';


    public function img_inmuebles()
    {
        return $this->hasMany(tbl_collection_img_inmuebles::class,'inmuebles_id', 'inmuebles_id');
    }
}
