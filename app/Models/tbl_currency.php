<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tbl_currency extends Model
{

    protected $fillable = [
        'currency_name',
        'currency_abr'
    ];

    protected $primarykey = 'currency_id';

}
