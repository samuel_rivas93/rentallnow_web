<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\tbl_renters;

class tbl_reservas extends Model
{
    protected $fillable = [
        'reservas_status',
        'reservas_llegada',
        'reservas_salida',
        'reservas_huespedes',
        'renters_id'
    ];

    protected $primarykey = 'reservas_id';

    public function renters()
    {
        return $this->hasMany(tbl_renters::class,'renters_id', 'renters_id');
    }
}
