<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tbl_renters;
use App\Models\tbl_hotel;
class DashboardController extends Controller
{

    public function index()
    {
        $title = [
            'primero' => 'Dashboard',
            'segundo' => 'Panel Principal',
        ];
        return view('home', compact('title'));
    }

    public function solicitudes_renters()
    {
        return tbl_renters::with('customers','user', 'category')->get();
    }

    public function detalle_renter(Request $request)
    {
        if ($request->category_id==1) {
            return tbl_hotel::with(['city','img_hotel'])->where('renters_id', $request->renters_id)->get();
        }
    }

    public function autorizacion_renters(Request $request)
    {
        $renter = tbl_renters::where('renters_id', $request->renters_id)->update([
            'renters_status' => 1
        ]);

        if ($renter == true) {
            return ['status' => 'success', 'message' => 'Renter Autorizado'];
        }
    }


}
