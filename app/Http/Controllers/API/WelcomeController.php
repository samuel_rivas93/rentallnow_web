<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\tbl_category;
use App\Models\tbl_inmuebles;
use App\Models\tbl_vehicles;
use Illuminate\Support\Facades\Validator;

class WelcomeController extends Controller
{
    public function categories(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        return tbl_category::all();
    }

    public function GetRenters(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $inmuebles = tbl_inmuebles::with('img_inmuebles')->get();
        $vehiculos = tbl_vehicles::with('img_vehicles')->get();

        return [
            'inmuebles' => $inmuebles,
            'vehiculos' => $vehiculos,
        ];
    }

    public function CardSearch(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
            
        if ($request->category_id == 1) {
            return tbl_inmuebles::with('img_inmuebles')->get();            
        }
    }

    public function SearchInmueble(Request $request)
    {
        $mensaje = [
            'access_token.required' => 'El código de autorización es obligatorio.',
        ];

        $validator = \Validator::make($request->all(),[
            'access_token' => 'required',
            'hotel_lng' => 'required',
            'reserva_llegada' => 'required',
            'reserva_salida' => 'required',
        ], $mensaje);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $inmueble = tbl_inmuebles::with('img_inmuebles')
                            ->where('inmuebles_lng', $request->hotel_lng)
                            ->whereNotExists(function($query) use($request){
                                $query->from('tbl_reservas')
                                      ->where('reservas_llegada','>=',  Carbon::parse($request->reserva_llegada))
                                      ->where('reservas_salida', '<=', Carbon::parse($request->reserva_salida))
                                      ->whereRaw('tbl_inmuebles.inmuebles_id = tbl_reservas.renters_id');
                            })->get(); 
        
        if ($inmueble == true) {
            return $inmueble;
        }else{
            return ['message' => 'No se encontro ningun inmueble para esas fechas'];
        }
    }

}
