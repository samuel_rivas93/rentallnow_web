<?php

namespace App\Http\Middleware;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;
use Closure;
use App\User;

class AdminMiddleware
{

    protected  $auth;
    public function __construct(Guard $auth){
        $this->auth = $auth;
    }


    public function handle($request, Closure $next)
    {
        if ($this->auth->check()) {
            switch($this->auth->user()->roles_id)
                {
                    case '1':
                       return Redirect::to('/cpanel');
                       break;
                   default:
                       return Redirect::to('/');
                       break;
               }
        }
        return $next($request);
    }


}
