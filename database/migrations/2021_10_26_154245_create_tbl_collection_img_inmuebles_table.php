<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCollectionImgInmueblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_collection_img_inmuebles', function (Blueprint $table) {
            $table->increments('collection_img_inmuebles_id');
            $table->longText('collection_img_inmuebles_url')->nullable();
            $table->integer('inmuebles_id')->unsigned();
            $table->timestamps();

            $table->foreign('inmuebles_id')->references('inmuebles_id')->on('tbl_inmuebles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_collection_img_inmuebles');
    }
}
