<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblCollectionImgServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_collection_img_services', function (Blueprint $table) {
            $table->increments('collection_img_services_id');
            $table->longText('collection_img_services_url')->nullable();
            $table->integer('services_id')->unsigned();
            $table->timestamps();

            $table->foreign('services_id')->references('services_id')->on('tbl_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_collection_img_services');
    }
}
