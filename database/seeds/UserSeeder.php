<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrador',
            'email' => 'admin@admin.com',
            'password' => Hash::make('123456'),
            'roles_id' => 1,
        ]);

        DB::table('users')->insert([
            'name' => 'Invitado',
            'email' => 'invitado@invitado.com',
            'password' => Hash::make('123456'),
            'roles_id' => 2,
        ]);
    }
}
