require('./bootstrap');
window.Vue = require('vue');


import VueRouter from 'vue-router';
import * as VueGoogleMaps from "vue2-google-maps";

Vue.use(VueRouter);

Vue.use(VueGoogleMaps, {
    load: {
      key: "AIzaSyCv2upnuUOVx8ZuEIP1Dfd0IG1q6q4ZkdU",
      libraries: "places" // necessary for places input
    }
  });
  
  

const routes = [
    {
        path:'/',
        component: require('./FrontLayout/Welcome.vue').default
    },

    {
        path:'/cpanel/solicitud',
        component: require('./BackLayout/Solicitudes_Renters.vue').default
    },

    {
        path:'/cpanel/categorias',
        component: require('./BackLayout/Categorias.vue').default
    },

    {
        path:'/cpanel/subcategorias',
        component: require('./BackLayout/SubCategorias.vue').default
    },

    {
        path:'/home',
        component: require('./BackLayout/Welcome.vue').default
    },

    {
        path:'/intro',
        component: require('./FrontLayout/Intro.vue').default
    },

    {
        path:'/property-type-group',
        name:'/property-type-group',
        component: require('./FrontLayout/Type_Group.vue').default
    },

    {
        path:'/inmuebles/property-type',
        name:'/inmuebles/property-type',
        component: require('./FrontLayout/inmuebles/Property_Type.vue').default
    },

    {
        path:'/inmuebles/privacy-type',
        name:'/inmuebles/privacy-type',
        component: require('./FrontLayout/inmuebles/Privacy_Type.vue').default
    },

    {
        path:'/inmuebles/location',
        name:'/inmuebles/location',
        component: require('./FrontLayout/inmuebles/Location.vue').default
    },

    {
        path:'/inmuebles/floor-plan',
        name:'/inmuebles/floor-plan',
        component: require('./FrontLayout/inmuebles/Floor-Plan.vue').default
    },

    {
        path:'/inmuebles/amenities',
        name:'/inmuebles/amenities',
        component: require('./FrontLayout/inmuebles/Amenities.vue').default
    },

    {
        path:'/inmuebles/photos',
        name:'/inmuebles/photos',
        component: require('./FrontLayout/inmuebles/Photos.vue').default
    },

    {
        path:'/inmuebles/title',
        name:'/inmuebles/title',
        component: require('./FrontLayout/inmuebles/Title.vue').default
    },

    {
        path:'/inmuebles/description',
        name:'/inmuebles/description',
        component: require('./FrontLayout/inmuebles/Description.vue').default
    },

    {
        path:'/inmuebles/price',
        name:'/inmuebles/price',
        component: require('./FrontLayout/inmuebles/Price.vue').default
    },

    {
        path:'/inmuebles/legal',
        name:'/inmuebles/legal',
        component: require('./FrontLayout/inmuebles/Legal.vue').default
    },

    {
        path:'/inmuebles/preview',
        name:'/inmuebles/preview',
        component: require('./FrontLayout/inmuebles/PreView.vue').default
    },

    {
        path:'/vehicles/list-your-car',
        name:'/vehicles/list-your-car',
        component: require('./FrontLayout/vehicles/List-Your-Car.vue').default
    },

]

const router = new VueRouter({
    routes: routes,
    mode: "history"
})

const app = new Vue({
    router
}).$mount('#app');